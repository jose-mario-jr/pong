import 'dart:math';
import 'dart:ui';

enum RectColArea {
  topLeft,
  top,
  topRight,
  right,
  bottomRight,
  bottom,
  bottomLeft,
  left
}

class GeometryMixin {
  bool isCircleWithinVerticalRange(double circleXOffset, double circleRadius,
          double boundary1XOffset, double boundary2XOffset) =>
      circleXOffset - circleRadius >= boundary1XOffset &&
      circleXOffset + circleRadius <= boundary2XOffset;

  bool isPointAboveHorizontalLine(double pointYOffset, double lineYOffset) =>
      pointYOffset > (lineYOffset / 2);

  bool isCircleBelowHorizontalLine(double circleYOffset, double circleRadius) =>
      circleYOffset + circleRadius < 0;

  bool isCircleAboveHorizontalLine(
          double circleYOffset, double circleRadius, double lineYOffset) =>
      circleYOffset - circleRadius > lineYOffset;

  bool isPointWithinRect(double pointX, double pointY, double rectXOffset,
          double rectYOffset, double rectHeight, double rectWidth) =>
      pointY >= rectYOffset &&
      pointY <= rectYOffset + rectHeight &&
      pointX >= rectXOffset &&
      pointX <= rectXOffset + rectWidth;

  RectColArea detectCircleRectCollision(
      Offset circleOffset,
      double circleRadius,
      Offset rectOffset,
      double rectHeight,
      double rectWidth) {
    List<Offset> samplePoints = calculateCircleCollisionDetectionSamplePoints(
        circleOffset, circleRadius);

    for (Offset p in samplePoints) {
      if (isPointWithinRect(
          p.dx, p.dy, rectOffset.dx, rectOffset.dy, rectHeight, rectWidth)) {
        return calculateRectColArea(
            p.dx, p.dy, rectOffset, rectHeight, rectWidth);
      }
    }
    return null;
  }

  List<Offset> calculateCircleCollisionDetectionSamplePoints(
      Offset circleOffset, double circleRadius) {
    List<Offset> samplePoints = new List<Offset>();

    for (double i = 0; i < 2 * pi; i += pi / 4) {
      Offset p = Offset(
        circleOffset.dx + (circleRadius * sin(i)),
        circleOffset.dy + (circleRadius * cos(i)),
      );
      samplePoints.add(p);
    }

    return samplePoints;
  }

  RectColArea calculateRectColArea(double pointX, double pointY,
      Offset rectOffset, double rectHeight, double rectWidth) {
    List<Offset> topSamplePoints = new List<Offset>();
    List<Offset> bottomSamplePoints = new List<Offset>();
    for (int i = 1; i < (rectWidth ~/ 10); i++) {
      topSamplePoints
          .add(Offset(rectOffset.dx + ((rectWidth / 10) * i), rectOffset.dy));
      bottomSamplePoints.add(Offset(
          rectOffset.dx + (rectWidth / 10) * i, rectOffset.dy + rectHeight));
    }

    Map<RectColArea, List<Offset>> samplePoints = {
      RectColArea.top: topSamplePoints,
      RectColArea.topRight: [Offset(rectOffset.dx + rectWidth, rectOffset.dy)],
      RectColArea.right: [
        Offset(rectOffset.dx + rectWidth, rectOffset.dy + (rectHeight / 2))
      ],
      RectColArea.bottomRight: [
        Offset(rectOffset.dx + rectWidth, rectOffset.dy + rectHeight)
      ],
      RectColArea.bottom: bottomSamplePoints,
      RectColArea.bottomLeft: [
        Offset(rectOffset.dx, rectOffset.dy + rectHeight)
      ],
      RectColArea.left: [
        Offset(rectOffset.dx, rectOffset.dy + (rectHeight / 2))
      ],
      RectColArea.topLeft: [Offset(rectOffset.dx, rectOffset.dy)],
    };

    RectColArea minDistanceArea;
    double minDistance = 100000;
    samplePoints.forEach((RectColArea rca, List<Offset> offsetList) {
      offsetList.forEach((Offset o) {
        double distance =
            calculateDistanceBetweenPoints(pointX, pointY, o.dx, o.dy);
        if (distance < minDistance) {
          minDistanceArea = rca;
          minDistance = distance;
        }
      });
    });
    return minDistanceArea;
  }

  double calculateDistanceBetweenPoints(
          double x1, double y1, double x2, double y2) =>
      sqrt(pow((x1 - x2), 2) + pow((y1 - y2), 2));
}
