# pong

A simple pong game developed to train my flutter skills

## where did it came from?

The game was based in the Marco Bramini's [flutter_pong](https://github.com/MarcoBramini/flutter_pong/tree/dump), under MIT License, and is being modified to meet the standards and requirements from the voices in my head. The code used was from the 'dump' branch and was pulled from it in 13/02/2021.
